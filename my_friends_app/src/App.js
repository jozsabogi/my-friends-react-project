import './App.css';
import MyFriendsPage from './components/MyFriendsPage';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  background: {
    background: 'rgb(189, 232, 212)',
    width: '100%',
    height: '100%'
  }
});

function App() {
  const classes = useStyles();
  return (
    <div className = {classes.background}>
      <MyFriendsPage/>
    </div>
  );
}

export default App;
