import { useEffect, useState, useCallback } from "react";
import Faker from 'faker';
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import IconButton from '@material-ui/core/IconButton';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';

import fetchFriends from '../services/FriendService';
import Friends from './Friends';
import Weather from './Weather';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  button: {
    background: '#026670',
    color: 'white',
    width: '50%'
  },
  buttonGroup: {
    marginTop: '20px',
    marginLeft: '10px',
    marginBottom: '20px',
  },
  margins: {
    marginTop: '10px',
    marginLeft: '30px'
  },
  horizontallLine: {
    background: '#026670',
    height: '5px'
  },
  details: {
    height: '40%'
  }
});

function createProposalFriends() {
  var randomFriends = [];
  for (var i=0; i<100; i++) {
    randomFriends.push({
      name : Faker.name.findName(),
      age: Faker.datatype.number({'min':10, 'max':80}),
      location: Faker.address.city(),
      image : Faker.image.avatar()
    });
  }
  return randomFriends;
}

function Content(props) {

  const friendType = props.friendType;
  const [showType, setShowType] = useState('icon');

  const [myFriends, setMyFriends] = useState([]);
  const [proposedFriends, setProposedFriends] = useState([]);

  useEffect(() => {
    const friendsRandom = createProposalFriends();
    setProposedFriends(friendsRandom);
  }, []);

  useEffect(() => {
    fetchFriends(setMyFriends);
  },[]);

  const classes = useStyles();

  const friendsPerPage = 8;
  const page = props.page;
  const setPage = props.setPage;

  const [selectedFriend, setSelectedFriend] = useState({});
  const detailsClicked = props.detailsClicked;
  const setDetailsClicked = props.setDetailsClicked;
  
  return (
    <div className={classes.margins}>
      <ButtonGroup className={classes.buttonGroup}>
        <Button className = {classes.button}
          onClick = {() => setShowType('icon')}
        >Icons</Button>
        <Button className = {classes.button}
           onClick = {() => setShowType('list')}
        >List</Button>
      </ButtonGroup>
      <div>
        <div>
          <Friends 
            friendType = {friendType}
            friends = {{
              myFriends: myFriends,
              proposedFriends: proposedFriends,
              setMyFriends: setMyFriends,
              setProposedFriends: setProposedFriends
            }}
            saveOrDelete = {friendType === 'personal' ? 'delete' : 'save'}
            showType = {showType}
            page = {page}
            friendsPerPage = {friendsPerPage}
            setSelectedFriend = {setSelectedFriend}
            setDetailsClicked = {setDetailsClicked}
          />
        </div>
        <div>
          <div>
            <IconButton onClick = {() => {
              if (page > 0)
                setPage(page-1);
            }}><NavigateBeforeIcon/></IconButton>

            <label>{page+1}</label>

            <IconButton onClick = {() => {
              if (page+1 < ((friendType === 'personal' ? myFriends.length : proposedFriends.length)/friendsPerPage)) {
                setPage(page+1);
              }
            }}><NavigateNextIcon/></IconButton>
          </div>
        </div>
      </div>
      <div className = {classes.horizontallLine}></div>
      <div className = {classes.details}>
        { detailsClicked && 
          <Weather friend = {selectedFriend}></Weather>
        }
      </div>
    </div>
  );
}

export default Content;