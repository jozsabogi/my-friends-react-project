import Avatar from '@material-ui/core/Avatar';
import Paper from "@material-ui/core/Paper";
import IconButton from '@material-ui/core/IconButton';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import PersonAddDisabledIcon from '@material-ui/icons/PersonAddDisabled';
import MoreVertIcon from '@material-ui/icons/MoreVert';

import { makeStyles} from '@material-ui/core/styles';

import fetchFriends, {saveFriend, deleteFriend} from '../services/FriendService';

const useStyles = makeStyles({
  paperIcon: {
    width: '20%',
    margin: '15px',
    display: 'inline-block',
    boxSizing: 'border-box',
    padding: '10px',
    color: '#026670'
  },
  showIcons: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row'
  },
  listItem: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    height: '40px',
    marginBottom: '10px'
  },
  iconHeader: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    background: '#DDDDDD'
  },
  center : {
    marginLeft: 'auto',
    marginRight: 'auto'
  }
});

function saveToDB(item) {
  saveFriend(item);
}

function deleteFromDB(item) {
  const id = item._id;
  deleteFriend(id);
}

function subList(friendList, page, elementPerPage) {
  var friends = [];
  const start = page * elementPerPage;
  const end = (page + 1) * elementPerPage;
  for (var i = start; i < end; i++){
    if (friendList[i])
      friends.push(friendList[i]);
  }
  return friends;
}

function refreshFriendList(setMyFriends) {
  fetchFriends(setMyFriends);
}

function Icons(props) {
  const myFriends = props.friends.myFriends;
  const proposedFriends = props.friends.proposedFriends;
  const setMyFriends = props.friends.setMyFriends;
  const setProposedFriends = props.friends.setProposedFriends;

  const friendType = props.friendType;
  const action = props.action;
  const page = props.page;
  const friendsPerPage = props.friendsPerPage;
  const setSelectedFriend = props.setSelectedFriend;
  const setDetailsClicked = props.setDetailsClicked;

  const classes = useStyles();

  const friendList = friendType === 'personal' ? myFriends : proposedFriends
  const actualFriendList = subList(friendList, page, friendsPerPage);

  const iconItems = actualFriendList.map((item) =>
    <Paper className = {classes.paperIcon} key = {`friend - ${item.name} - ${item.age}`}>
      <div className = {classes.iconHeader}>
        { friendType === 'personal' &&
          <IconButton onClick = {() => { 
              setSelectedFriend(item);
              setDetailsClicked(true);}}>
            <MoreVertIcon/>
          </IconButton>
        }
         <Avatar alt='FriendImage' src={item.image} className = {classes.center}></Avatar>
         <IconButton
            onClick={() => {
              if (action === 'delete') {
                deleteFromDB(item);
                alert(`${item.name} removed`);
                refreshFriendList(setMyFriends);
              } else {
                saveToDB(item);
                alert(`${item.name} added`);
                refreshFriendList(setMyFriends);

                const index = proposedFriends.indexOf(item);
                if (index > -1) {
                  proposedFriends.splice(index, 1);
                }

                setProposedFriends(proposedFriends);
              }
              setDetailsClicked(false);
            }
          }
          >{action === 'delete' ? <PersonAddDisabledIcon/> : <PersonAddIcon/>}</IconButton>
      </div>
      <label>Name: {item.name}</label> <br/>
      <label>Age: {item.age}</label> <br/>
      <label>Location: {item.location}</label> <br/>
    </Paper>
  );

  return iconItems;
}

function List(props) {
  const myFriends = props.friends.myFriends;
  const proposedFriends = props.friends.proposedFriends;
  const setMyFriends = props.friends.setMyFriends;
  const setProposedFriends = props.friends.setProposedFriends;

  const friendType = props.friendType;
  const action = props.action;
  const page = props.page;
  const friendsPerPage = props.friendsPerPage;
  const setSelectedFriend = props.setSelectedFriend;
  const setDetailsClicked = props.setDetailsClicked;

  const friendList = friendType === 'personal' ? myFriends : proposedFriends
  const actualFriendList = subList(friendList, page, friendsPerPage);

  const classes = useStyles();

  const listItems = actualFriendList.map((item) =>
    <div key = {`friend - ${item.name} - ${item.age}`}>
      <div className = {classes.listItem}>
        <Avatar alt='FriendImage' src={item.image} style = {{width: '30px', height: '30px', marginRight: '10px'}}></Avatar>
        <div>
          <label>Name: {item.name} • </label>
          <label>Age: {item.age} • </label>
          <label>Location: {item.location} </label>
          { friendType === 'personal' &&
          <IconButton onClick = {() => { 
              setSelectedFriend(item);
              setDetailsClicked(true);}}>
            <MoreVertIcon/>
          </IconButton>         
        }
        <IconButton
            onClick={() => {
              if (action === 'delete') {
                deleteFromDB(item);
                alert(`${item.name} removed`);
                refreshFriendList(setMyFriends);
              } else {
                saveToDB(item);
                alert(`${item.name} added`);
                refreshFriendList(setMyFriends);

                const index = proposedFriends.indexOf(item);
                if (index > -1) {
                  proposedFriends.splice(index, 1);
                }

                setProposedFriends(proposedFriends);
              }
              setDetailsClicked(false);
            }
          }
          >{action === 'delete' ? <PersonAddDisabledIcon/> : <PersonAddIcon/>}</IconButton>
        </div>
      </div>
    </div>
  );

  return listItems;
}

function Friends(props) {
  const classes = useStyles();

  const friendType = props.friendType;
  const myFriends = props.friends.myFriends;
  const proposedFriends = props.friends.proposedFriends;
  const setMyFriends = props.friends.setMyFriends;
  const setProposedFriends = props.friends.setProposedFriends;
  const action = props.saveOrDelete;
  const showType = props.showType;
  const page = props.page;
  const friendsPerPage = props.friendsPerPage;
  const setSelectedFriend = props.setSelectedFriend;
  const setDetailsClicked = props.setDetailsClicked;

  return (
    <div className = {showType === 'icon' ? classes.showIcons : {}}>
      {showType === 'icon' ? 
        <Icons 
          friends = {{
            myFriends: myFriends,
            proposedFriends: proposedFriends,
            setMyFriends: setMyFriends,
            setProposedFriends: setProposedFriends
          }}
          friendType = {friendType} 
          action={action} 
          page = {page} 
          friendsPerPage = {friendsPerPage} 
          setSelectedFriend = {setSelectedFriend}
          setDetailsClicked = {setDetailsClicked}/> : 
        <List 
          friends = {{
            myFriends: myFriends,
            proposedFriends: proposedFriends,
            setMyFriends: setMyFriends,
            setProposedFriends: setProposedFriends
          }} 
          friendType = {friendType} 
          action={action} 
          page = {page} 
          friendsPerPage = {friendsPerPage} 
          setSelectedFriend = {setSelectedFriend}
          setDetailsClicked = {setDetailsClicked}/>
      }
    </div>
  );
}

export default Friends;