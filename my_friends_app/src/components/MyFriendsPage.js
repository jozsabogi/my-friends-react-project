import Content from "./Content";
import {useState} from 'react';
import SideBar from './SideBar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  allignHorizontally: {
    width: '100%',
    alignContent: 'center',
    display: 'flex',
    flexDirection: 'row'
  },
  sideBar: {
    width: '20%'
  },
  contentSpace: {
    width: '75%'
  },
  background: {
    //background: 'linear-gradient(45deg, #43C6AC 30%, #F8FFAE 90%)'
    background: '#F7FDFF'
  },
  verticalLine: {
    background: '#026670',
    marginTop: '20px',
    width: '5px'
  }
});


function MyFriendsPage() {

  const [friendType, setFriendType] = useState('personal');
  const classes = useStyles();

  const [page, setPage] = useState(0);
  const [detailsClicked, setDetailsClicked] = useState(false);
  
  return (
    <div className={classes.background}>
      <div className={classes.allignHorizontally}>
        <div className={classes.sideBar}>
          <SideBar 
            setFriendType = {setFriendType} 
            setPage = {setPage} 
            setDetailsClicked = {setDetailsClicked}/>
        </div>
        <div className={classes.verticalLine}/>
        <div className={classes.contentSpace}>
          <Content 
            friendType={friendType} 
            page = {page} 
            setPage = {setPage} 
            detailsClicked = {detailsClicked} 
            setDetailsClicked = {setDetailsClicked}/>
        </div>
      </div>
    </div>
  );
}

export default  MyFriendsPage;