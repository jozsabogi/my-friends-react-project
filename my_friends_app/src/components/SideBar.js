import Button from "@material-ui/core/Button";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  button: {
    background: '#026670',
    border: 0,
    borderRadius: 5,
    color: 'white',
    width: '100%',
    marginBottom: '10px'
  },
  margins: {
    marginTop: '100px',
    marginRight: '10px',
    marginLeft: '10px'
  }
});

function SideBar(props) {

  const setFriendType = props.setFriendType;
  const setPage = props.setPage;
  const setDetailsClicked = props.setDetailsClicked;

  const classes = useStyles();
  
  return (
    <div className={classes.margins}>
      <Button 
        className={classes.button}
        onClick={() => {setFriendType('personal'); setPage(0); setDetailsClicked(false);}}
      > My Personal Friends </Button>
      <br/>
      <Button 
        className={classes.button}
        onClick={() => {setFriendType('proposed'); setPage(0); setDetailsClicked(false);}}
      > Proposed Friends </Button>
    </div>
  );
}

export default  SideBar;