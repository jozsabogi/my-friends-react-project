import { useEffect, useState} from "react";
import { makeStyles } from '@material-ui/core/styles';

async function getWeather(cityName, apiKey) {
  const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${apiKey}`);
  let weatherInfo;
  if (response.ok){
    weatherInfo = await response.json();
  }

  return weatherInfo;
}

const useStyles = makeStyles({
  container: {
    marginTop: '50px',
    marginLeft: '30px',
    display: 'flex'
  },
  fixed: {
    width: '25%'
  },
  flex_item: {
    flexGrow: 1
  },
  weatherInfo: {
    color: '#026670', 
    marginTop: '10px'
  },
  detailsBold : {
    color: '#026670', 
    fontWeight: 'bold'
  }
});

function Weather(props) {
  const friend = props.friend;
  const classes = useStyles();

  const apiKey = '005337c6c5794970172e6cf494c7b693';

  const [weatherInfo, setWeatherInfo] = useState();

  useEffect(async () => {
    const weatherInfoData = await getWeather(friend.location, apiKey);
    setWeatherInfo(weatherInfoData);
  }, [friend]);

  return (
    <div className = {classes.margins}>
      <div className = {classes.container}>
        <div className = {classes.fixed} >
          <div className = {classes.detailsBold}>
            <img alt='FriendImage' src={friend.image} style = {{width: '200px', height: '150px', borderRadius: 10, objectFit: 'cover'}}></img><br/>
            <label>Name: {friend.name}</label><br/>
            <label>Age: {friend.age}</label><br/>
            <label>Location: {friend.location}</label>
          </div>
        </div>
        <div style = {{marginTop: '10px'}}>
          <label className = {classes.detailsBold}>Weather in {friend.location}:</label><br/>
          {
            weatherInfo ? 
              <div className = {classes.weatherInfo}>
                <label>Description: {weatherInfo.weather[0].description}</label><br/>
                <label>Temperature: {weatherInfo.main.temp} °F</label><br/>
                <label>Wind: {weatherInfo.wind.speed} m/s</label><br/>
                <label>Humidity: {weatherInfo.main.humidity}% </label>
              </div> :
              <div className = {classes.weatherInfo}>
                <label>Sorry, we can't get weather info about {friend.location} :(</label>
              </div>
            }
        </div>
      </div>
    </div>
  );
  
}

export default Weather;