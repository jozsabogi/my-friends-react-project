function fetchFriends(setMyFriends) {
  fetch('http://localhost:3030/')
    .then ((response) => response.json())
    .then ((friends) => setMyFriends(friends));
}

function saveFriend(friend) {
  fetch('http://localhost:3030/', {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(friend),
  })
  .then(() => console.log('Friend Inserted'));
}

function deleteFriend(id) {
  fetch(`http://localhost:3030/delete/${id}`, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
    }
  })
  .then(() => console.log('Friend Deleted'));
}


export default fetchFriends;
export {saveFriend};
export {deleteFriend};