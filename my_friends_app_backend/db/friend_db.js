const mongodb = require('mongodb'); 
const MongoClient = mongodb.MongoClient;

const Friend = require('../models/friend'); 

const mongoUrl = 'mongodb+srv://jbim1827:mongodb@mydatabase.lei53.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const mongoDbNamespace = 'MyFriendsApp';

var client;

async function connectDB() { 
	if (!client) client = await MongoClient.connect(mongoUrl, { useUnifiedTopology: true });
    console.log('Create a DB connection object');
	return { 
		db: client.db(mongoDbNamespace), 
		client: client
	};
}

// read all friends
async function readFriends(){
    const {db, client} = await connectDB();
    const collection = db.collection('friendCollection');
    const document = await collection.find({});
    const friendsArray = await document.toArray();
    return friendsArray;
}

// insert friend to database
async function insertFriend(friend){
    const {db, client} = await connectDB();
    const collection = db.collection('friendCollection');
    await collection.insertOne(friend);
}

// delete friend from database (by id)
async function deleteFriend(id){
    const {db, client} = await connectDB();
    const collection = db.collection('friendCollection');
    const myquery = { _id: new mongodb.ObjectID(id) };
    try {
        await collection.deleteOne(myquery);
    } catch(error) {
        throw error;
    }
}

module.exports = {
    readFriends, 
    insertFriend,
    deleteFriend
};