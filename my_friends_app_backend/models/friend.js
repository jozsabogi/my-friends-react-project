function Friend(name, age, location, image) {
  this.name = name;
  this.age = age;
  this.location = location;
  this.image = image;
}

module.exports = Friend;