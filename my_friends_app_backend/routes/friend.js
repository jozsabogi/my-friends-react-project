const express = require('express');
const router = express.Router();

const {readFriends, insertFriend, deleteFriend} = require('../db/friend_db');

router.get('/', async function(req, res, next) {
    const friends = await readFriends()
    console.log('GET all friends');
    res.send(friends);
});

router.post('/', async function(req, res, next) {
    await insertFriend(req.body)
    console.log(`POST insert friend: ${JSON.stringify(req.body)}`);
    res.send('Inserted correctly');
});

router.delete('/delete/:id', async function(req, res, next) {
    await deleteFriend(req.params.id)
    console.log(`DELETE friend with id: ${req.params.id}`);
    res.send('Deleted correctly');
});

module.exports = router;